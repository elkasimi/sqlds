var SQLSchema = function (name, model, datastore) {
	this.sourceName = datastore.name;
	this.name = name;
	var entityManager = datastore.getEntityManager();
	
	this.getEntityManager = function() {
		return entityManager;
	};	

	var dataClasses = {};
	var tableNames = Object.getOwnPropertyNames(model);

	var that = this;

	tableNames.forEach(function (tableName) {
		dataClasses[tableName] = new SQLDataClass(that, tableName, model[tableName]);

		Object.defineProperty(that, tableName, {
			enumerable: true,
			configurable: true,
			set: function (value) {
				//no setter
			},
			get: function () {
				return dataClasses[tableName];
			}
		});
	});
};
