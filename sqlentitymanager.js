var SQLEntityManager = function (inVersionType) {
	/*
	** getVersionManager
	*/
	var versionManager = null;
	var versionType = inVersionType || "orm";

	if(versionManager == null) {
		if(versionType == "orm") {
			versionManager = new ORMVersionManager();
		} else if(versionType == "trigger") {
			versionManager = new TriggerVersionManager();
		} else if(versionType == "hash") {
			versionManager = new HashVersionManager();
		} else {
			throw new Error("Unknown version manager type!");
		}
	}

	this.getVersionType = function() {
		return versionType;
	};

	/*
	** createEntity
	*/
	this.createEntity = function(dataclass, primaryKey) {
		var entity = new SQLEntity(dataclass, primaryKey);
		var version = versionManager.getVersion(entity);
		entity.setVersion(version);

		return entity;
	};

	/*
	** saveEntity
	*/
	this.saveEntity = function(entity) {
		if(versionType == "orm") {
			//if this a retrieved entity
			//check if the version is the same as the one saved in the entity manager
			var kind = entity.getKind();
			if(kind == "retrieved") {
				if(entity.getVersion() != versionManager.getVersion(entity)) {
					throw new Error("This entity has been changed in the server!");
				}
			}

			if(kind == "retrieved") {
				getSessionManager().save(dataclass, entity.getChangedValues(), entity.getPrimaryKey());
			} else {
				getSessionManager().saveNew(dataclass, entity.getValues());
			}

			versionManager.updateVersion(entity);
			var version = versionManager.getVersion(entity);
			entity.setVersion(version);
		} else if(versionType == "hash") {
			//save the entity only if the same version as the server
			var kind = entity.getKind();
			var version = entity.getVersion();
			if(kind == "retrieved") {
				var data = getSessionManager().saveWithChecksumVerification(dataclass, entity.getValues(), entity.getPrimaryKey(), version);
				var affectedRowCount = data.affectedRowCount;
				var newVersion = data.newVersion;
				if(affectedRowCount != 1) {
					throw new Error("This entity has been changed in the server!");
				}
				entity.setVersion(newVersion);
			} else {
				getSessionManager().saveNew(dataclass, entity.getValues());
			}


		} else if(versionType == "trigger") {
			/*
			** TODO
			*/
		} else {
			throw new Error("Unknown version type!");
		}
	};
};

