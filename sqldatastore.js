var SQLDataStore = function (name, model, versionType) {
	this.name = name;
	var entityManager = null;
	
	this.getEntityManager = function() {
		if(entityManager == null) {
			entityManager = new SQLEntityManager(versionType);
		}
		
		return entityManager;
	};
	
	var sqlschemas = {};
	var that = this;
	var schemaNames = Object.getOwnPropertyNames(model);
	schemaNames.forEach(function (schemaName) {
		sqlschemas[schemaName] = new SQLSchema(schemaName, model[schemaName], that);
		Object.defineProperty(that, schemaName, {
			enumerable: true,
			configurable: true,
			set: function (value) {
				//no setter
			},
			get: function () {
				return sqlschemas[schemaName];
			}
		});
	});
};

