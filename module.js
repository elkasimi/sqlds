exports.sqlds = (function() {
	var sqlds = {};

	//for all sources in sqlsources folder
	//	params = JSON.parse(strfile);
	//	session = sql.connect(params);
	//	model = sql.createModel(params)
	//	append source name to sqlds
	//
	//

	folder = Folder(solution.getFolder().path + 'sqlsources');

	if (folder.exists) {
		folder.forEachFile(function (file) {
			var params, contents, sourceName;

			params = JSON.parse(file.toString());
			model = sql.createModel(params);
			sourceName = file.nameNoExt;
			getSessionManager().addSession(sourceName, params);
			sqlds[sourceName] = new SQLDataStore(sourceName, model, params.versionType);
		});
	}

	return sqlds;
})();

