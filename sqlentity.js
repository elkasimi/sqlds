var SQLEntity = function (dataclass, primaryKey) {
	var values = {};
	var changedValues = {};
	var gotten = false;
	var kind = "created";
	var saved = false;
	var removed = false;
	var that = this;

	this.sourceName = dataclass.sourceName;
	this.schemaName = dataclass.schemaName;
	this.dataclassName = dataclass.name;
	
	/*
	** getEntityManager
	*/
	var entityManager = dataclass.getEntityManager();
	
	this.getEntityManager = function() {
		return entityManager;
	}
	
	var version;

	if(primaryKey) {
		kind = "retrieved";
	} else {
		primaryKey = dataclass.getPrimaryKey();
		kind = "created";
	}

	dataclass.getAttributesNames().forEach(function(a) {
		Object.defineProperty(that, a, {
			enumerable : true,
			configurable : true,
			set : function(value) {
				changedValues[a] = value;
				values[a] = value;
				saved = false;
			},
			get : function() {
				if(kind == "retrieved") {
					if(!gotten) {
						var that = this;
						var attrs = dataclass.getLightAttributesNames();
						var o = getSessionManager().getFirst(dataclass, attrs, primaryKey);
						
						Object.getOwnPropertyNames(o).forEach(function (a) {
							if(values[a] === undefined) {
								values[a] = o[a];
							}
						});
						
						gotten = true;
					}
					
					if(values[a] === undefined) {
						var o = getSessionManager().getFirst(dataclass, a, pkey);
						values[a] = o[a];
					}
				}
				
				return values[a];
			}
		});
	});
	
	this.getKind = function() {
		return kind;
	};
	
	/*
	** setVersion
	*/
	this.setVersion = function(newVersion) {
		version = newVersion;
	};

	/*
	** setVersion
	*/
	this.getVersion = function() {
		return version;
	};

	/*
	** getKey
	** the key is a uniq reference to be used by the entity manager
	*/
	this.getKey = function() {
		var key = dataclass.schemaName + dataclass.name + JSON.stringify(primaryKey);
		return key;
	};

	/*
	** save
	*/
	this.save = function() {
		if(!saved) {
			entityManager.saveEntity(this);
			changedValues = {};
			saved = true;
		}
	};
	
	/*
	** getValues
	*/
	this.getValues = function() {
		return values;
	};
	
	/*
	** getChangedValues
	*/
	this.getChangedValues = function() {
		return changedValues;
	};
	
	this.getPrimaryKey = function() {
		return primaryKey;
	};

	/*
	** remove
	*/
	this.remove = function() {
		if(!removed) {
			if(kind == "retrieved") {
				getSessionManager().remove(dataclass, primaryKey);
				removed = true;
			}
		}
	};
	
	/*
	** getAttributesNames
	*/
	this.getAttributesNames = function() {
		return dataclass.getAttributesNames();
	};
};

