var sql = require('waf-sql');

var sessionManager = null;

var getSessionManager = function () {
	if(sessionManager == null) {
		sessionManager = new SQLSessionManager();
	}

	return sessionManager;
};

var SQLSessionManager = function() {
	var sessions = {};
	var params = {};

	/*
	** addSession
	*/
	this.addSession = function(sourceName, inParams) {
		params[sourceName] = inParams;
	};

	/*
	** getSession
	*/
	this.getSession = function(sourceName) {
		if(sessions[sourceName] === undefined) {
			//if this._sessions[sourceName].isConnected
			var sourceParams = params[sourceName];
			sessions[sourceName] = sql.connect(sourceParams);
		}

		var session = sessions[sourceName];
		return session;
	};

	/*
	** getPrimaryKey
	*/
	this.getPrimaryKey = function(dataclass, filter) {
		var session = this.getSession(dataclass.sourceName);
		var key = dataclass.getPrimaryKey();
		var attributes = Object.getOwnPropertyNames(key).join(",");
		var primaryKey = session.find(attributes, dataclass.name, filter);

		return primaryKey;
	};

	/*
	** getPrimaryKeys
	*/
	this.getPrimaryKeys = function(dataclass, filter) {
		var session = this.getSession(dataclass.sourceName);
		var key = dataclass.getPrimaryKey();
		var attributes = Object.getOwnPropertyNames(key).join(",");
		var res = session.select(attributes, dataclass.name, filter);
		var primaryKeys = res.getAllRows();

		return primaryKeys;
	};

	/*
	** getFirst
	*/
	this.getFirst = function(dataclass, attributes, filter) {
		var session = this.getSession(dataclass.sourceName);
		var row = session.find(attributes.join(","), dataclass.name, filter);

		return row;
	};
	
	/*
	** getFirstWithChecksum
	*/	
	this.getFirstWithChecksum = function(dataclass, attributes, filter) {
		var query = "select ";
		query += attributes.join(',');
		
		query += "sha1(concat(";
		var allAttributes = dataclass.getAttributesNames();
		query += allAttributes.join(',');
		query += ")) as checksum";
		query += " from " + dataclass.name + " where ";
		Object.getOwnPropertyNames(filter).forEach(function(a) {
			query += "`" + a + "` = ";
			var value = filter[a];
			if(typeof value == "number") {
				query += value + " ";
			} else if(value instanceof Date) {
				query += "'" + value.getUTCFullYear() + "-" + (value.getUTCMonth() + 1) + "-" + value.getUTCDate();
				query += " " + value.getUTCHours() + ":" + value.getUTCMinutes() + ":" + value.getUTCSeconds() + ".";
				query += value.getUTCMilliseconds();
			} else {
				query += "'" + value + "' ";
			}
		});
		
		var session = this.getSession(dataclass.sourceName);
		var res = session.execute(query);
		var row = res.getNextRow();
		
		return row;
	};	

	/*
	** getAll
	*/
	this.getAll = function(dataclass, attributes, filter) {
		var session = this.getSession(dataclass.sourceName);
		var res = session.select(attributes.join(","), dataclass.name, filter);
		var rows = res.getAllRows();

		return rows;
	};


	/*
	** save
	*/
	this.save = function(dataclass, values, filter) {
		var session = this.getSession(dataclass.sourceName);
		session.update(dataclass.name, values, filter);
	};


	/*
	** saveNew
	*/
	this.saveNew = function(dataclass, values) {
		var session = this.getSession(dataclass.sourceName);
		session.insert(dataclass.name, [values]);
	};
	
	this.saveWithChecksumVerification = function(dataclass, values, filter, checksum) {
		var query = "update `" + dataclass.name + "` set ";
		var first = true;
		Object.getOwnPropertyNames(values).forEach(function(a) {
			if(first)
				first = false;
			else
				query += ",";
			
			query += "`" + a + "` = ";
			var value = values[a];
			if(typeof value == "number") {
				query += value + " ";
			} else if(value instanceof Date) {
				query += "'" + value.getUTCFullYear() + "-" + (value.getUTCMonth() + 1) + "-" + value.getUTCDate();
				query += " " + value.getUTCHours() + ":" + value.getUTCMinutes() + ":" + value.getUTCSeconds() + ".";
				query += value.getUTCMilliseconds() + "'";
			} else {
				query += "'" + value + "' ";
			}
		});
		
		query += " where ";
		
		var strFilterQuery = "";
		first = true;
		Object.getOwnPropertyNames(filter).forEach(function(a) {
			if(first)
				first = false;
			else
				strFilterQuery += ",";
			
			strFilterQuery += "`" + a + "` = ";
			var value = filter[a];
			if(typeof value == "number") {
				strFilterQuery += value + " ";
			} else if(value instanceof Date) {
				strFilterQuery += "'" + value.getUTCFullYear() + "-" + (value.getUTCMonth() + 1) + "-" + value.getUTCDate();
				strFilterQuery += " " + value.getUTCHours() + ":" + value.getUTCMinutes() + ":" + value.getUTCSeconds() + ".";
				strFilterQuery += value.getUTCMilliseconds() + "'";
			} else {
				strFilterQuery += "'" + value + "' ";
			}
		});
		
		query += strFilterQuery;
		
		query += " and ";
		query += "sha1(concat(" + dataclass.getAttributesNames().join(',') + ")) = '" + checksum + "';";
		query += "select sha1(concat(" + dataclass.getAttributesNames().join(',') + ")) as newVersion ";
		query += "from " + dataclass.name + " ";
		query += "where ";
		query += strFilterQuery;

		var session = this.getSession(dataclass.sourceName);
		var res = session.execute(query);
		var affectedRowCount = res.getAffectedRowCount();
		res = session.getNextResult();
		var row = res.getNextRow();
		return {
			affectedRowCount : affectedRowCount,
			newVersion : row.newVersion
		};
	};


	/*
	** remove
	*/
	this.remove = function(dataclass, filter) {
		var session = this.getSession(dataclass.sourceName);
		session.delete(dataclass.name, filter);
	};

	/*
	** getCount
	*/

	this.getCount = function(dataclass) {
		var session = this.getSession(dataclass.sourceName);
		var count = session.getCount(dataclass.name);
		return count;
	};
};

