var SQLDataClass = function (schema, name, model) {
	this.sourceName = schema.sourceName;
	this.schemaName = schema.name;
	this.name = name;
	this.count = getSessionManager().getCount(this);

	/*
	** getEntityManager
	*/
	var entityManager = schema.getEntityManager();
	
	this.getEntityManager = function() {
		return entityManager;
	}
	
	/*
	** getPrimaryKey
	*/
	this.getPrimaryKey = function () {
		var keys = {};
		Object.getOwnPropertyNames(model).forEach(function (a) {
			if (model[a].isPrimaryKey) {
				keys[a] = null;
			}
		});

		return keys;
	};
	/*
	** getLightAttributesNames
	*/
	this.getLightAttributesNames = function () {
		var attrs = [];
		Object.getOwnPropertyNames(model).forEach(function (a) {
			if (model[a].type !== "blob") {
				attrs.push(a);
			}
		});

		return attrs;
	};
	/*
	** getAttributesNames
	*/
	this.getAttributesNames = function () {
		var attrs = [];
		Object.getOwnPropertyNames(model).forEach(function (a) {
			attrs.push(a);
		});

		return attrs;
	};
	/*
	** find
	*/
	this.find = function(filter) {
		var primaryKey = getSessionManager().getPrimaryKey(this, filter);
		var entity = entityManager.createEntity(this, primaryKey);
		return entity;
	};
	/*
	** query
	*/
	this.query = function(filter) {
		var collection = new SQLEntityCollection(this, filter);

		return collection;
	};
	/*
	** createEntity
	*/
	this.createEntity = function(values) {
		var entity = new SQLEntity(this);

		if (values) {
			Object.getOwnPropertyNames(values).forEach(function(p) {
				entity[p] = values[p];
			});
		}

		return entity;
	};
	/*
	** all function
	*/
	this.all = function() {
		var collection = new SQLEntityCollection(this, "");
		return collection;
	};
	
};
