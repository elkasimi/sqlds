//we suppose mysqlsource is in sqlsources under the solution folder containing this:
// {
// 	"dbType" : "mysql",
// 	"hostname" : "localhost",
// 	"port" : 3306,
// 	"database" : "benchdb",
// 	"ssl" : false,
// 	"username" : "root",
// 	"password" : "secret"
// }

function testFind() {
    var sqlds = require('waf-sqlds').sqlds;
    var e = sqlds.mysqlsource.benchdb.people.find('id = 15');

    Y.Assert.areSame(e.id, 15);
    Y.Assert.areSame(e.matricule, 15);
    Y.Assert.areSame(e.first_name, 15);
    Y.Assert.areSame(e.last_name, 15);
    Y.Assert.areSame(e.date_of_birth, 15);
}
