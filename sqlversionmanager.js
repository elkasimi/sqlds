var ORMVersionManager = function() {
	var entityVersion = {};

	this.getVersion = function(entity) {
		var key = entity.getKey();
		var version = 0;
		if(entityVersion.hasOwnProperty(key)) {
			version = entityVersion[key];
		}

		return version;
	};

	this.updateVersion = function(entity) {
		var key = entity.getKey();
		var version = 0;
		if(entityVersion.hasOwnProperty(key)) {
			version = entityVersion[key];
		}

		entityVersion[key] = version + 1;
	};
};

var HashVersionManager = function() {
	this.getVersion = function(entity) {
		var session = getSessionManager().getSession(entity.sourceName);
		var fields = "sha1(concat(" + entity.getAttributesNames().join(',') + ")) as version";
		var row = session.find(fields, entity.dataclassName, entity.getPrimaryKey());
		var hash = row.version;
		
		return hash;		
	};
};

var TriggerVersionManager = function() {
	this.getVersion = function(entity) {
		/*
		** TODO
		*/
	};

	this.updateVersion = function(entity) {
		/*
		** TODO
		*/
	};
};

