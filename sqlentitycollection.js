var SQLEntityCollection = function (dataclass, queryPath) {
	//if we want pagination
	//this.count = this._entitieskeys.length;

	this.queryPath = queryPath || '';
	this.queryPlan = '';

	var entitiesKeys = getSessionManager().getPrimaryKeys(dataclass, queryPath);
	this.length = entitiesKeys.length;

	//this.count = getSessionManager().getCount(dataclass);

	var currentEntity = 0;

	var that = this;

	dataclass.getAttributesNames().forEach(function(name) {
		Object.defineProperty(that, name, {
			enumerable: true,
			configurable: true,
			set: function (value) {
			},
			get: function () {
				var res = [];
				var rows = getSessionManager().getAll(dataclass, [name], queryPath);
				rows.forEach(function(row) {
					res.push(row[name]);
				});
				return res;
			}
		});
	});

	/*
	 * count 
	 */
	this.count = function() {
		return this.length;
	};

	/*
	** query
	*/
	this.query = function (filter) {};

	/*
	** first
	*/
	this.first = function() {
		var e = null;

		if(this.length > 0) {
			var key = entitiesKeys[0];
			e = new SQLEntity(dataclass, key);
		}

		return e;
	};
};

